const config = require('../../nightwatch.conf.js');

module.exports = {
  Neti(browser) {
    browser
      .url('https://neti.ee/')
      .windowSize('current', 1000, 1000)
      .waitForElementVisible('body')
      .assert.containsText('.avaleht-content', 'Riik ja Ühiskond')
      .saveScreenshot(`${config.imgpath(browser)}neti-avaleht.png`)
      .useXpath()
      .click('//*[@id="main-content"]/div[1]/div[2]/p[4]/a[8]')
      .pause(500)
      .saveScreenshot(`${config.imgpath(browser)}neti-turvalisus.png`)
      .end();
  },
};
