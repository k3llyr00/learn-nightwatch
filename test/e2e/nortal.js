const config = require('../../nightwatch.conf.js');

module.exports = {
  Nortal(browser) {
    browser
      .url('https://nortal.com/')
      .waitForElementVisible('body')
      .windowSize('current', 1000, 1000)
      .saveScreenshot(`${config.imgpath(browser)}cookies-window.png`)
      .assert.elementPresent('#onetrust-consent-sdk')
      .click('#onetrust-reject-all-handler')
      .pause(500)
      .saveScreenshot(`${config.imgpath(browser)}page-after-cookies.png`)
      .pause(500)
      .end();
  },
};
