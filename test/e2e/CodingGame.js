/*

> Open page
>Check if the cookie banner exists?
>Click on more opinions
>validate if checkboxes are correctly checked
>accept privacy settings
>Scroll down
>Click on "Level up skills"
>Close login banner
>Skip tutorial banner
>Open Settings menu
>Change theme from dark to light
>Go back to main page
*/

const config = require('../../nightwatch.conf.js');

module.exports = {
  CodingGame(browser) {
    browser
      .url('https://www.codingame.com/start')
      .waitForElementVisible('body')
      .windowSize('current', 1000, 1000)
      .saveScreenshot(`${config.imgpath(browser)}codinggame-banner.png`)
    browser.verify.visible('.cg-cookies-banner')
      .pause(1000)
      .useXpath()
      .click('//*[@id="content"]/cg-cookies-banner/div/div/div/p[2]/span/button') // cookies options
      .saveScreenshot(`${config.imgpath(browser)}codinggame-banner-options.png`)
      .pause(1000);
    browser.expect.element('/html/body/div[7]/div/div/div/div/ng-include/div/cg-confidentiality-settings-form/div/form/div[1]/div/cg-checkbox').to.have.attribute('checked'); // analytics checked?
    browser.expect.element('/html/body/div[7]/div/div/div/div/ng-include/div/cg-confidentiality-settings-form/div/form/div[2]/div/cg-checkbox').to.not.have.attribute('checked'); // facebook unchecked?
    browser.expect.element('/html/body/div[7]/div/div/div/div/ng-include/div/cg-confidentiality-settings-form/div/form/div[3]/div/cg-checkbox').to.not.have.attribute('checked'); // advertisment unchecked?
    browser.click('/html/body/div[7]/div/div/div/div/ng-include/div/cg-confidentiality-settings-form/div/form/button') // accept privacy settings
      .pause(1000)
      .saveScreenshot(`${config.imgpath(browser)}start-page.png`)
      .click('//*[@id="start"]/section[1]/button/div/div') // scroll down
      .pause(1000)
      .saveScreenshot(`${config.imgpath(browser)}scroll-page.png`)
      .click('//*[@id="start"]/section[3]/div[2]/article/a') // level up skill button
      .pause(1000)
      .saveScreenshot(`${config.imgpath(browser)}levelupskills.png`)
      .click('/html/body/cg-login-popup/div/div/div/div/div[1]') // continue without login
      .pause(1000)
      .saveScreenshot(`${config.imgpath(browser)}login.png`)
      .click('/html/body/div[6]/div[5]/div/button') // skip tutorial
      .pause(1000)
      .saveScreenshot(`${config.imgpath(browser)}notutorial.png`)
      .click('//*[@id="scrollable-pane"]/div/ide-page/div/div[2]/div[3]/div/div[3]/div[5]/button') // open settings
      .pause(1000)
      .saveScreenshot(`${config.imgpath(browser)}settings.png`)
      .click('//*[@id="scrollable-pane"]/div/ide-page/div/div[2]/div[4]/div[2]/div[2]/div/div/div/div[1]/div/label[2]') // edit theme
      .pause(1000)
      .saveScreenshot(`${config.imgpath(browser)}lighttheme.png`)
      .click('//*[@id="scrollable-pane"]/div/ide-page/div/div[2]/div[3]/div/div[3]/div[1]/a') // back to main page
      .pause(1000)
      .saveScreenshot(`${config.imgpath(browser)}startpage-end.png`)
      .end();
  },
};
